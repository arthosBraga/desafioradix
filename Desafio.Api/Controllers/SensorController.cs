﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Domain.Commands.SensorCommands.Inputs;
using Desafio.Domain.Commands.SensorCommands.Outputs;
using Desafio.Domain.Entities;
using Desafio.Domain.Handlers;
using Desafio.Domain.Queries;
using Desafio.Domain.Repositories;
using Desafio.Domain.VO;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
    public class SensorController : Controller
    {
        private readonly ISensorRepository _repository;
        private readonly SensorHandler _handler;
        public SensorController(ISensorRepository repository,SensorHandler handler)
        {
            _repository = repository;
            _handler =  handler;
        }
        [HttpGet]
        [Route("v1/api/sensors")]        
        public IEnumerable<ListSensorQueryresult> Get()
        {

            return _repository.Get();
        }
        [HttpPost]

        [Route("v1/api/sensor")]
        public object Post([FromBody]CreateSensorCommand T)
        {

            var result = (CreateSensorCommandResult)_handler.Handle(T);
            if (_handler.Invalid)
                return BadRequest(_handler.Notifications);
                
            return result;
        }
        
    }
}
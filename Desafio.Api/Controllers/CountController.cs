﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Desafio.Domain.Entities;
using Desafio.Domain.Queries;
using Desafio.Domain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Desafio.Api.Controllers
{
    public class CountController : Controller
    {
        private readonly ISensorRepository _repository;
        public CountController(ISensorRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]
        [Route("v1/api/sensor/country/{country}")]
        public CountCountryQueryresult GetCountry(string country)
        {
            return _repository.GetCountry(country) ;
        }

        [HttpGet]
        [Route("v1/api/sensor/region/{region}")]
        public CountRegionQueryresult GetRegion(string region)
        {
            return _repository.GetRegion(region);
        }



        [HttpGet]
        [Route("v1/api/sensor/sensor/{sensor}")]
        public CountSensorQueryresult GetCountSensor(string sensor)
        {
            return _repository.GetCountSensor(sensor);
        }


    }
}
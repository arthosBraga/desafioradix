﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Mvc;
using Desafio.Domain.Repositories;
using Desafio.Infra.Repositories;
using Desafio.Infra.DataContexts;
using Desafio.Domain.Handlers;
using Swashbuckle.AspNetCore.Swagger;

namespace Desafio.Api
{
    public class Startup
    {
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddScoped<DataContext, DataContext>();
            services.AddTransient<ISensorRepository, SensorRepository>();
            services.AddTransient<SensorHandler, SensorHandler>();

            services.AddSwaggerGen(x =>
            {
                x.SwaggerDoc("v1", new Info { Title = "Sensor API", Version = "v1" });
            });
        }

       
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v1/swagger.json", "Sensor API - V1");
            });
        }
    }
}

﻿using Desafio.Domain.Entities;
using Desafio.Domain.Queries;
using System.Collections.Generic;

namespace Desafio.Domain.Repositories
{
    public interface ISensorRepository
    {
        // Sensor
        void Save(Event sensor);       
        IEnumerable<ListSensorQueryresult> Get();

        // Count
        CountCountryQueryresult GetCountry(string country);
        CountRegionQueryresult GetRegion(string region);
        CountSensorQueryresult GetCountSensor(string sensor);
    }

}

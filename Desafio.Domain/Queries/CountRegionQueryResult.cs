﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Queries
{
    public class CountRegionQueryresult
    {
        public string Country { get; set; }
        public string Region{ get; set; }
        public string Count { get; set; }
        
    }
}

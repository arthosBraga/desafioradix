﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Queries
{
    public class CountSensorQueryresult
    {
        public string Country { get; set; }
        public string Region { get; set; }
        public string Sensor { get; set; }
        public int Count { get; set; }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Queries
{
    public class ListSensorQueryresult
    {
        public System.Guid Id { get; set; }
        public string Sensor{ get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
        public DateTime Time { get; set; }
        public string Valor { get; set; }
        public string Error { get; set; }
    }
}

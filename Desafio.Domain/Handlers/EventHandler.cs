﻿using Desafio.Domain.Commands.SensorCommands.Inputs;
using Desafio.Domain.Commands.SensorCommands.Outputs;
using Desafio.Domain.Entities;
using Desafio.Domain.Repositories;
using Desafio.Domain.VO;
using Desafio.Shared.Commands;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Handlers
{
    public class SensorHandler : Notifiable, ICommandHandler<CreateSensorCommand>
    {
        private readonly ISensorRepository _repository;
        public SensorHandler(ISensorRepository repository)
        {
            _repository = repository;
        }
        public ICommandResult Handle(CreateSensorCommand command)
        {
            if (command != null)
            {
                var timestamp = new TimeStampVO(command.TimeStamp);
                var tag = new Tag(command.Tag);
                var value = new ValueVO(command.Value);

                var sensor = new Event(timestamp, tag, value);

                AddNotifications(timestamp.Notifications);
                AddNotifications(tag.Notifications);
                AddNotifications(value.Notifications);
                AddNotifications(sensor.Notifications);

                _repository.Save(sensor);

                return new CreateSensorCommandResult(sensor.Id, timestamp.ToString(), tag.ToString(), value.ToString());
            }
            else
                AddNotification("Sensor", "Is Null");
                return null;
        }
        }
    }


﻿using Desafio.Shared.Commands;
using FluentValidator;
using FluentValidator.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Commands.SensorCommands.Outputs
{
    public class CreateSensorCommandResult : Notifiable, ICommandResult
    {
        public CreateSensorCommandResult() { }
        public CreateSensorCommandResult(Guid id, string timeStamp, string tag, string value)
        {
            
                Id = id;
                TimeStamp = timeStamp;
                Tag = tag;
                Value = value;
            
            
        }

        public Guid Id { get; set; }
        public string TimeStamp { get; set; }
        public string Tag { get; set; }
        public string Value { get; set; }

        public bool IsValid()
        {
            AddNotifications(new ValidationContract()
                .HasMaxLen(TimeStamp, 10, "TimeStamp", "TimeStamp must contain at least 10 characters")
                .IsNotNull(TimeStamp, TimeStamp, "Null Value")

            );
            return Valid;
        }
    }
}

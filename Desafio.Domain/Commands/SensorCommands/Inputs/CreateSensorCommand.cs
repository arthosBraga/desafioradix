﻿using Desafio.Shared.Commands;
using FluentValidator;
using FluentValidator.Validation;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Commands.SensorCommands.Inputs
{
   public class CreateSensorCommand : Notifiable, ICommand
    {
        
        public string TimeStamp { get;  set; }
        public string Tag { get;  set; }
        public string Value { get;  set; }

        public bool IsValid()
        {
            AddNotifications(new ValidationContract()                
                .HasMaxLen(TimeStamp, 10, "TimeStamp", "O TimeStamp deve conter pelo exatos 10 caracteres")
                .IsNotNull(TimeStamp, TimeStamp, "Null Value")
                
            );
            return Valid;
        }
    }    
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.Entities
{
    class EventCount
    {
        public EventCount(
            string identifier,
            int count
            )
        {

            Identifier = identifier;
            Count = count;

        }



        public string Identifier { get; private set; }
        public int Count { get; private set; }
       
        public override string ToString()
        {
            return $"{Identifier}";
        }
    }
}

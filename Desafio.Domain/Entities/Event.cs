﻿using Desafio.Domain.VO;
using Desafio.Shared.Entities;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Desafio.Domain.Entities
{
   public class Event : Entity
    {       
     
        public Event(
            TimeStampVO timeStamp,
            Tag tag,
            ValueVO value)
        {

            TimeStamp = timeStamp;
            if(tag.Valid)
            Tag = tag;
            if(tag.Valid)
            Value = value;
            if (timeStamp.Invalid)
                AddNotification("Sensor", "Error inserting timeStamp");
            if (tag.Invalid)
                AddNotification("Sensor", "Error inserting Tag");
            if (value.Invalid)
                AddNotification("Sensor", "Error inserting Value");
            
        }


        
        public TimeStampVO TimeStamp { get; private set; }
        public Tag Tag { get; private set; }           
        public ValueVO  Value { get; private set; }
        public override string ToString()
        {
            return $"{Tag}";
    }
    }
   
}

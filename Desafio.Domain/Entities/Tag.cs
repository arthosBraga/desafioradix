﻿using Desafio.Shared.Entities;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace Desafio.Domain.Entities
{
    public class Tag : Entity
    {
        public Tag(string t)
        {
            if (!string.IsNullOrEmpty(t))
            {
                string[] g;
                g = Regex.Split(t, "\\.");
                if (g.Length == 3)
                {
                    Country = g[0];
                    Region = g[1];
                    Name = g[2];
                }
                else
                    AddNotification("Tag", "Unexpected tag format.");
            }
            else
                AddNotification("Tag", "Tag Not Null");
        }

        public Tag(string country, string region, string name)
        {
            Country = country;
            Region = region;
            Name = name;
        }

        public string Country { get; private set; }
        public string Region { get; private set; }
        public string Name { get; private set; }
        public override string ToString()
        {
           
                return $"{Country}.{Region}.{Name}";
            
        }
    }
    }

﻿using Desafio.Shared.Entities;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.VO
{
    public class TimeStampVO : Entity
    {
        public TimeStampVO(string timeStamp)
        {
            if (!string.IsNullOrEmpty(timeStamp) && timeStamp.Length <= 10 )
            {
                if (!string.IsNullOrEmpty(timeStamp) && decimal.TryParse(timeStamp, out decimal numValue))
                {

                    if (int.Parse(timeStamp) < 2147483647)
                    {
                        int tmp = int.Parse(timeStamp);
                        Time = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(tmp).ToUniversalTime();
                    }
                    else
                        AddNotification("TimeStamp", "Incorrect value Above that allowed for Unix TimeStamp.");

                }
                else
                    AddNotification("TimeStamp", "Error in input format");
            }
            else
                AddNotification("TimeStamp", "Erro of Legth");
            
        }

        public DateTime Time { get; private set; }
        public override string ToString()
        {

            return $"{Time}";
        }
    }
}

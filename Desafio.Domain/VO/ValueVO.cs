﻿using Desafio.Shared.Entities;
using FluentValidator;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Domain.VO
{
    public class ValueVO : Notifiable
    {
        public ValueVO(decimal value, Boolean error)
        {
            Value = value;
            Error = error;
        }
        public ValueVO(string t)
        {
            if (string.IsNullOrEmpty(t) && string.IsNullOrWhiteSpace(t))
            {
                Error = true;
                AddNotification("Value", "Empty field");
            }

            else if (!decimal.TryParse(t, out decimal numValue))
            {
                Error = true;
                AddNotification("Value", "Characters not valid");
            }
            else
                Value = decimal.Parse(t);


        }
        public decimal Value { get; private set; }
        public bool Error { get; private set; }

        public override string ToString()
        {
            if (!Error)
                return $"{Value}";
            else
                return $"Error";
        }
    }
}

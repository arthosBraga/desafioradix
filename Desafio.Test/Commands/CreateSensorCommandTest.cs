﻿using Desafio.Domain.Commands.SensorCommands.Inputs;
using Desafio.Domain.Entities;
using Desafio.Domain.VO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace Desafio.Test.Commands
{
    [TestClass]
    public class CreateSensorCommandTest
    {
        [TestMethod]
        public void ShouldValidateWhenComandIsValid()
        {
            var command = new CreateSensorCommand();
            command.Tag = "Brasil.Sudeste.Sensor01";
            command.TimeStamp = "1557341073";
            command.Value = "22342342";

            Assert.AreEqual(command.Tag, "Brasil.Sudeste.Sensor01");
            Assert.AreEqual(command.TimeStamp, "1557341073");
            Assert.AreEqual(command.Value, "22342342");
        }
    }
}

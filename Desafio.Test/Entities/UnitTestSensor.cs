
using Desafio.Domain.Entities;
using Desafio.Domain.VO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desafio.Test
{
    [TestClass]
    public class SensorTest
    {
        [TestMethod]
        public void TestSensorOk()
        {
            var tag = new Tag("Brasil.Sudeste.Sensor01");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag,value);
            
            Assert.AreEqual(true, S.Valid);

        }

        [TestMethod]

        public void TestSensorTimeEmpty()
        {
            var tag = new Tag("Brasil.Sudeste.Sensor01");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("");
            var S = new Event(time, tag, value);

            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorTimeInvalidValue()
        {
            var tag = new Tag("Brasil.Sudeste.Sensor01");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("asddes2233");
            var S = new Event(time, tag, value);

            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorErrorTag()
        {
            var tag = new Tag("Brasil.Sul.oeste.Sensor01");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorErrorTagEmpty()
        {
            var tag = new Tag("");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorErrorTagNumber()
        {
            var tag = new Tag("23523242");
            var value = new ValueVO("1557270593");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorErrorValueEmpty()
        {
            var tag = new Tag("Brasil.Suldeste.Sensor01");
            var value = new ValueVO("");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorErrorValueNotNumber()
        {
            var tag = new Tag("Brasil.Suldeste.Sensor01");
            var value = new ValueVO("abc");
            var time = new TimeStampVO("1557284106");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Invalid);

        }

        [TestMethod]
        public void TestSensorValueOk()
        {
            var tag = new Tag("Brasil.Suldeste.Sensor01");
            var value = new ValueVO("34434332234");
            var time = new TimeStampVO("1557341073");
            var S = new Event(time, tag, value);
            Assert.AreEqual(true, S.Valid);

        }

       
    }
}

CREATE DATABASE [bd_desafio_radix]

USE [bd_desafio_radix]
GO


CREATE TABLE [dbo].[Sensor](
	[Id] [uniqueidentifier] NOT NULL,
	[Sensor] [varchar](50) NOT NULL,
	[Region] [varchar](50) NOT NULL,
	[Country] [varchar](50) NOT NULL,
	[Time] [datetime] NOT NULL,
	[Valor] [decimal](18, 4) NULL,
	[Error] [bit] NULL,
 CONSTRAINT [PK_Sensor] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[CountEvent](
	[IdCount] [uniqueidentifier] NOT NULL,
	[Identfier] [varchar](50) NOT NULL,
	[count] [int] NOT NULL
) ON [PRIMARY]
GO


﻿using Desafio.Domain.Entities;
using Desafio.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Desafio.Infra.DataContexts;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Desafio.Domain.Queries;

namespace Desafio.Infra.Repositories
{
    public class SensorRepository : ISensorRepository
    {
        private readonly DataContext _context;

        public SensorRepository(DataContext context)
        {
            _context = context;
        }

        public IEnumerable<ListSensorQueryresult> Get()
        {
            return
                _context
                .Connection
                .Query<ListSensorQueryresult>("Select [Id],[Sensor],[Region],[Country],[Time],[Valor],[Error] FROM [Sensor]", new {});
        }

        public CountCountryQueryresult GetCountry(string country)
        {
            return
                _context
                .Connection
                .Query<CountCountryQueryresult>("Select [Country],COUNT(*)AS [Count] from [Sensor] where [Country]=@country group by Country,[Country]", new { country = country })
                .FirstOrDefault();
        }

        public CountSensorQueryresult GetCountSensor(string sensor)
        {
            return
                _context
                .Connection
                .Query<CountSensorQueryresult>("Select [Country],[Region],[Sensor],COUNT(*)AS [Count] from [Sensor] where [sensor]=@sensor group by [sensor],[Country],[Region]", new { sensor = sensor})
                .FirstOrDefault();
        }

        public CountRegionQueryresult GetRegion(string region)
        {
            return
                _context
                .Connection
                .Query<CountRegionQueryresult>("Select [Country],[Region],COUNT(*)AS [Count] from [Sensor] where [Region]=@region group by Country,[Region]", new { region = region })
                .FirstOrDefault();
            
        }
                

        public void Save(Event sensor)
        {
            if (sensor.Valid)
            {
                _context
                    .Connection
                    .Execute(@"INSERT INTO [dbo].[Sensor] 
                            ([Id],
                             [Sensor],
                             [Region],
                             [Country],
                             [Time],
                             [Valor],
                             [Error]) 
                             VALUES
                            (@Id,@Sensor,@Region,@Country,@Time,@Valor,@Error)", new
                    {
                        Id = sensor.Id,
                        Sensor = sensor.Tag.Name,
                        Region = sensor.Tag.Region,
                        Country = sensor.Tag.Country,
                        Time = sensor.TimeStamp.Time,
                        Valor = sensor.Value.Value,
                        Error = sensor.Value.Error
                    });
            }
            
                
        }
    }
}
